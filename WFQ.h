class WeightedQuickUnionQF {
private:
    int *parent; // parent array
    int *size_arr; // size array
    int size;

    int root(int p) {
        while (p != parent[p]) {
            parent[p] = parent[parent[p]];
            p = parent[p];
        }
        return p;
    }

public:
    WeightedQuickUnionQF(int n)
        : parent{new int[n]}, size{n}, size_arr{new int[n]} {
            for (int i = 0; i < n; i++) {
                parent[i] = i;
                size_arr[i] = 1;
            }
        }
    ~WeightedQuickUnionQF() {
        delete[] parent;
        delete[] size_arr;
    }

    bool is_connected(int p, int q) {
        return (root(p) == root(q));
    }

    void connect(int p, int q) {
        int rootp = root(p);
        int rootq = root(q);

        parent[rootq] = parent[rootp];

        if (size_arr[rootp] > size_arr[rootq]) {
            parent[rootq] = rootp;
            size_arr[rootp] += size_arr[rootq];
        } else {
            parent[rootp] = parent[rootq];
            size_arr[rootq] += size_arr[rootp];
        }
    }
};