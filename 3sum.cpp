#include <iostream>
#include <vector>
#include <algorithm>

int threeSum(int numbers[], int size, int sum)
{
    // sort the numbers
    std::sort(numbers, numbers + size);
    // check if array is sorted
    for (auto i = 1; i < size; i++) {
        if (numbers[i] < numbers[i - 1]) {
            printf("Array not sorted\n");
            return -1;
        }
    }

    auto count = 0;
    for (auto i = 0; i < size; i++) {
        // reduce it to a fast two sum problem
        auto sum1 = -numbers[i];
        auto j = i + 1;
        auto k = size - 1;
        while (j < k) {
            auto sum2 = numbers[j] + numbers[k];
            //printf("sum1: %d\n", sum1);
            if (sum1 == sum2) {
                count++;
                //printf("sum1: %d\n", sum1);
                j++;
                k--;
            }
            else if (sum2 < sum1)
                j++;
            else
                k--;
        }
    }
    return count;
}

int main(int argc, char *argv[])
{
    int size = 0;
    sscanf(argv[1], "%dK", &size);
    size *= 1000;
    int *numbers = new int[size];
    FILE *fp = fopen(argv[1], "r+");
    if (fp != nullptr) {
        for (auto i = 0; i < size; i++)
            fscanf(fp, "%d", &numbers[i]);
        printf("%d\n", threeSum(numbers, size, 0)); 
    } else {
        printf("fp is null\n");
    }
    
}