#include "node.h"
template <typename T>
class Stack
{
private:
    Node<T> *head;
    int length = 0;
public:
    Stack() : head{nullptr} {};
    ~Stack()
    {
        auto nodes_freed = 0;
        for (Node<T> *p = head; head != nullptr;)
        {
            Node<T> *freeptr = head;
            head = head->next;
            delete freeptr;
            nodes_freed++;
        }
    }
    void push (T val)
    {
        Node<T> *n = new Node(val);
        n->next = head;
        head = n;
        length++;
    }

    T pop()
    {
        Node<T> *n = head;
        head = head->next;
        T val{n->element};
        delete n;
        length--;
        return val;
    }
    T peek()
    {
        return head->element;
    }

    int getLength() { return length; }
};