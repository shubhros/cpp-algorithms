#include <iostream>

template <typename T>
class Node
{
public:
    T value;
    Node<T> *next;
    Node<T> *prev;

    Node(T val) : value{val}, next{nullptr} {}
};

template <typename T>
class List
{
    Node<T> *head;
    Node<T> *tail;

public:
    List() : head{nullptr}, tail{nullptr} {}
    List(Node<T> *head, Node<T> *tail) : head{head}, tail{tail} {}
    List(const List<T> &l)
    {
        head = nullptr;
        tail = nullptr;
        for (Node<T> *p = l.head; p != NULL; p = p->next)
        {
            push_back(p->value);
        }
    }

    void push_back(T val)
    {
        Node<T> *v = new Node<T>(val);
        if (head == nullptr && tail == nullptr)
        {
            head = v;
            tail = v;
        }
        else
        {
            tail->next = v;
            v->prev = tail;
            tail = v;
        }
    }

    void insert_front(T val)
    {
        Node<T> *v = new Node<T>(val);
        if (head == nullptr && tail == nullptr)
        {
            head = v;
            tail = v;
        }
        else
        {
            v->next = head;
            v->next->prev = v;
            //head->prev = v;
            head = v;
        }
    }

    T pop_back()
    {
        T val;
        Node<T> *v;
        if (head == tail)
        {
            val = head->value;
            v = head;
            head = nullptr;
            tail = nullptr;
        }
        else
        {
            v = tail;
            val = v->value;
            tail = tail->prev;
            tail->next = nullptr;
        }
        delete v;
        return val;
    }
    T remove_front()
    {
        T val;
        Node<T> *v;
        if (head == tail)
        {
            val = head->value;
            v = head;
            head = nullptr;
            tail = nullptr;
        }
        else
        {
            val = head->value;
            v = head;
            head = head->next;
            head->prev = nullptr;
        }
        delete v;
        //std::cout << "Removed: " << val << " " << head << " " << tail << " " << isEmpty() << std::endl;
        return val;
    }

    Node<T> *findMid()
    {
        Node<T> *fast, *slow;
        for (fast = head, slow = head; fast != NULL && fast->next != NULL && fast->next->next != NULL; fast = fast->next->next, slow = slow->next)
            ;
        return slow;
    }

    List<T> splitMid()
    {
        List<T> second;
        // find the mid -
        Node<T> *mid = findMid();
        second.head = mid->next;
        second.tail = tail;
        second.head->prev = nullptr;
        // current list ends at mid
        tail = mid;
        tail->next = nullptr;
        return second;
    }

    void dump()
    {
        for (auto p = head; p != NULL; p = p->next)
        {
#ifdef PRINT_POINTERS
            std::cout << p->value << "(" << p << ")"
                      << "-->";
#else
            std::cout << p->value << "-->";
#endif
        }
        std::cout << "#" << std::endl;
    }

    bool isEmpty()
    {
        return (head == nullptr);
    }

    List<T> mergeLists(List<T> &l2)
    {
        List<T> result{List<T>()};
        if (isEmpty())
        {
            return l2;
        }
        if (l2.isEmpty())
        {
            return *this;
        }
        while (!isEmpty() || !l2.isEmpty())
        {
            if (isEmpty())
            {
                result.push_back(l2.remove_front());
            }
            else if (l2.isEmpty())
            {
                result.push_back(remove_front());
            }
            else if (head->value < l2.head->value)
            {
                result.push_back(remove_front());
            }
            else
            {
                result.push_back(l2.remove_front());
            }
        }
        return result;
    }

    List<T> sort()
    {
        // sort list using merge sort
        if (isEmpty() || head == tail)
            return *this;
        auto mid = splitMid();
        auto s1 = this->sort();
        auto s2 = mid.sort();
        return s1.mergeLists(s2);
    }
};

int main()
{
    int n;
    int value;
    std::cin >> n;
    List<int> l1;
    //int *a = new int[n];

    for (auto i = 0; i < n; i++)
    {
        std::cin >> value;
        //l1.push_back(value);
        l1.insert_front(value);
    }
    auto l2 = l1.sort();
    l2.dump();
    /*for (auto i = 0; i < n; i++)
    {
        l1.remove_front();
        l1.dump();
    }*/

    /*    l1.dump();
    List<int> l2 = l1;
    l2.dump();

    List<int> l3 = l1.mergeLists(l2);

    l3.dump();*/
    //List<int> half = l1.splitMid();
    //l1.dump();
    //half.dump();
    //l1.dump();
    //auto l2 = l1.sort();
    //l2.dump();
}