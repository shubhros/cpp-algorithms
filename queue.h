#include "node.h"

template <typename T>
class Queue
{
private:
    Node<T> *head, *tail;
    int length = 0;
public:
    Queue() : head{nullptr}, tail{nullptr} {}
    ~Queue()
    {
        for (Node<T> *p = head; head != nullptr;) {
            Node<T> *freeptr = head;
            head = head->next;
            delete freeptr;
        }
    }
    void enqueue(T val)
    {
        Node<T> *n = new Node<T>(val);
        if (length == 0) {
            head = n;
            tail = n;
        } else {
            tail->next = n;
            tail = n;
        }
        length++;
    }
 
    bool dequeue(T& val)
    {
        bool retval = false;
        if (length > 0) {
            Node<T> *n = head;
            head = head->next;
            val = n->element;
            delete n;
            if (length == 0)
                tail = nullptr;
            length--;
            retval = true;
        }
        return retval;
    }
    
    int getLength() { return length; }
};