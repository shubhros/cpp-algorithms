#include <iostream>
#include <limits>
#include <stdexcept>
#include <vector>
template <typename T>
void selectionSort(T *a, int size)
{
    // Loop invariant - find the minimum value in the
    // array from i to N . Swap the minimum value to
    // arr[i]
    for (auto i = 0; i < size; i++)
    {
        int min = i;
        for (auto j = i; j < size; j++)
        {
            if (a[j] < a[min])
            {
                min = j;
            }
        }
        // exchange a[i], a[min]
        T swap = a[i];
        a[i] = a[min];
        a[min] = swap;
    }
}

class Point
{
private:
    int x;
    int y;

public:
    int getx() { return x; }
    int gety() { return y; }
    Point()
    {
        this->x = 0;
        this->y = 0;
    }
    Point(int x, int y)
    {
        this->x = x;
        this->y = y;
    }
    void display()
    {
        printf("(%d, %d)\n", x, y);
    }
    std::string toStr()
    {
        char b[50];
        snprintf(b, 50, "(%d, %d)", x, y);
        return std::string(b);
    }
    bool operator<(Point &that)
    {
        if (y < that.y ||
            (y == that.y && x < that.x))
        {
            return true;
        }
        return false;
    }
    bool operator==(Point &q)
    {
        return (x == q.x && y == q.y);
    }

    double slopeTo(Point &that)
    {
        double negative_infinity = -std::numeric_limits<double>::infinity();
        double positive_infinity = std::numeric_limits<double>::infinity();
        if (x == that.x)
        {
            if (y == that.y)
                return negative_infinity;
            return positive_infinity;
        }
        else if (y == that.y)
        {
            return 0;
        }
        else
        {
            return (double)(that.y - y) / (double)(that.x - x);
        }
    }
    int compareBySlope(Point &a, Point &b)
    {
        double s1 = slopeTo(a);
        double s2 = slopeTo(b);
        if (s1 < s2)
            return -1;
        else if (s1 > s2)
            return 1;
        else
            return 0;
    }
};

class LineSegment
{
private:
    Point p;
    Point q;

public:
    LineSegment(Point p, Point q)
    {
        if (p == q)
            throw std::invalid_argument("both points are same point");
        this->p = p;
        this->q = q;
    }

    bool operator==(LineSegment &that)
    {
        return (p == that.p && q == that.q);
    }

    void print()
    {
        std::cout << "[(" << p.getx() << "," << p.gety() << ") --> "
                  << "(" << q.getx() << "," << q.gety() << ")]" << std::endl;
    }
};

void printPoints(std::vector<Point> &points)
{
    std::cout << "[";
    for (auto p : points)
        std::cout << "(" << p.getx() << "," << p.gety() << ")";
    std::cout << "]" << std::endl;
}

void printPoints(Point points[4])
{
    std::cout << "[";
    for (auto i = 0; i < 4; i++)
        std::cout << "(" << points[i].getx() << "," << points[i].gety() << ")";
    std::cout << "]" << std::endl;
}

bool isCollinear(Point &p1, Point &p2, Point &p3)
{
    auto s1{p1.slopeTo(p2)};
    auto s2{p1.slopeTo(p3)};

    return (s1 == s2);
}

class BruteCollinearPoints
{
private:
    std::vector<Point *> fourPointCollinear;
    std::vector<LineSegment> segments;
    std::vector<LineSegment> same_segments;

    bool isCollinear(Point &p1, Point &p2, Point &p3, Point &p4)
    {
        auto s1{p1.slopeTo(p2)};
        auto s2{p1.slopeTo(p3)};
        auto s3{p1.slopeTo(p4)};

        return (s1 == s2 && s2 == s3);
    }

public:
    BruteCollinearPoints(std::vector<Point> points)
    {
        // find the set of 4 points which are collinear
        for (auto i = 0; i < points.size(); i++)
        {
            for (auto j = i + 1; j < points.size(); j++)
            {
                // fill in the line segments here
                segments.push_back(LineSegment(points[i], points[j]));
                for (auto k = j + 1; k < points.size(); k++)
                {
                    for (auto l = k + 1; l < points.size(); l++)
                    {
                        if (isCollinear(points[i], points[j], points[k], points[l]))
                        {
                            Point fp[4] = {points[i], points[j], points[k], points[l]};
                            fourPointCollinear.push_back(fp);
                            // need to remove the segments creaded fy points[i..j..k..l]
                            same_segments.push_back(LineSegment(points[i], points[j]));
                            same_segments.push_back(LineSegment(points[j], points[k]));
                            same_segments.push_back(LineSegment(points[k], points[l]));
                            same_segments.push_back(LineSegment(points[i], points[l]));
                            same_segments.push_back(LineSegment(points[i], points[k]));
                            same_segments.push_back(LineSegment(points[j], points[l]));
                        }
                    }
                }
            }
        }
    }
    std::vector<LineSegment> &segment_list()
    {
        return segments;
    }

    std::vector<LineSegment> &same_segment_list()
    {
        return same_segments;
    }

    std::vector<Point *> &collinear()
    {
        return fourPointCollinear;
    }
};

int main()
{
    int n;
    std::cin >> n; // number of points
    int x, y;
    std::vector<Point> points;
    for (auto i = 0; i < n; i++)
    {
        std::cin >> x;
        std::cin >> y;
        points.push_back(Point(x, y));
    }
    BruteCollinearPoints b{BruteCollinearPoints(points)};
    auto collinear = b.collinear();
    for (auto ps : collinear)
        printPoints(ps);
    auto segments = b.segment_list();
    auto same_segments = b.same_segment_list();
    auto same_segment_count = 0;
    for (auto s : segments)
    {
        // look for segments in the same segment list
        bool found = false;
        for (auto ss : same_segments)
        {
            if (ss == s)
            {
                found = true;
                same_segment_count++;
                break;
            }
        }
        if (!found)
            s.print();
        // need to add collinear point maximal segments
    }
    std::cout << "Total number of same segments: " << same_segment_count << std::endl;
    for (auto c : collinear)
    {
        selectionSort(c, 4);
        // add the line segments
        auto l{LineSegment(c[0], c[3])};
        segments.push_back(l);
        l.print();
    }
}