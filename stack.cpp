#include <iostream>
#include "stack.h"
#include "queue.h"

int main()
{
    Stack<std::string> s1{Stack<std::string>()};
    Queue<std::string> q1{Queue<std::string>()};
    std::string arr[10] = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
    for (auto i = 0; i < 10; i++) {
        s1.push(arr[i]);
        q1.enqueue(arr[i]);

    }

    std::string s;
    while(q1.dequeue(s))
        std::cout << "dequeue: " << s << std::endl;
    return 0;
}
