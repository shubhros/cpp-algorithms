#ifndef __NODE_H_
#define __NODE_H_
template <typename T>
class Node {
public:
    T element;
    Node <T> *next;
    Node(T val) : element{val}, next{nullptr} {}
};
#endif