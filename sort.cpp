#include <iostream>

// selection sort to be implemented
template <typename T>
void selectionSort(T a[], int size)
{
    // Loop invariant - find the minimum value in the
    // array from i to N . Swap the minimum value to 
    // arr[i]
    for (auto i = 0; i < size; i++) {
        T min = i;
        for (auto j = i; j < size; j++) {
            if (a[j] < a[min]) {
                min = j;
            }
        }
        // exchange a[i], a[min]
        T swap = a[i];
        a[i] = a[min];
        a[min] = swap;
    }
}

template <typename T>
void swap(T& a, T&b)
{
    T swap = a;
    a = b;
    b = swap;
}
template <typename T>
void insertionSort(T *a, int size)
{
    // Loop invariant - at ith iteration find the 
    // correct place for a[i] by exchanging a[i] with
    // any larger element on its left. At any point in 
    // time, the subarray to the left of i is sorted
    for (auto i = 0; i < size; i++) {
        auto val = a[i];
        auto j = 0;
        for (j = i; j >= 0; j--) {
            if (a[j-1] > val) {
                //swap(a[j-1], a[j]);
                a[j] = a[j-1];
            }
        }
        a[j] = val;
    }
}

template <typename T>
void shellSort(T a[], int size)
{
    int h = 1;
    while(h < size/3) h = 3*h + 1;

    while (h >= 1) {
        // h sort the array
        for(int i = h; i < size; i++) {
            auto val = a[i];
            auto j = i;
            for (; j >= h; j -= h) {
                if (a[j-h] > val) {
                    a[j] = a[j-h];
                }
            }
            a[j] = val;
        }
        h /= 3;
    }
}

template <typename T>
std::string checkSorted(T a[], int size)
{
    for (auto i = 1; i < size; i++) {
        if (a[i-1] > a[i])
            return "not sorted";
    }
    return "sorted";
}

template <typename T>
void merge(T a[], T aux[], int low, int mid, int high)
{
    // copy over subarrays to aux array
    for (auto k = low; k <= high; k++)
        aux[k] = a[k];
    auto i = low;
    auto j = mid + 1;
    for (auto k = low; k <= high; k++)
    {
        if (i > mid)
            a[k] = aux[j++];
        else if (j > high)
            a[k] = aux[i++];
        else if (aux[i] < aux[j])
            a[k] = aux[i++];
        else
            a[k] = aux[j++];
    }
}

template <typename T>
void mergeSortHelper(T a[], T aux[], int low, int high)
{
    if (low >= high)
        return;
    auto mid = low + (high - low) / 2;
    mergeSortHelper(a, aux, low, mid);
    mergeSortHelper(a, aux, mid + 1, high);
    merge(a, aux, low, mid, high);
}

template <typename T>
void mergeSort(T a[], int size)
{
    T *aux = new T[size];
    mergeSortHelper(a, aux, 0, size - 1);
}


/*
int main()
{
    int n;
    std::cin >> n;
    //int *a = new int[n];
    std::string *a = new std::string[n];
    for (auto i = 0; i < n; i++) {
        std::cin >> a[i];
    }
    std::cout << "checking for sorted array: " << checkSorted(a, n) << std::endl;
    //insertionSort(a, n);
    //selectionSort(a, n);
    //shellSort(a, n);
    mergeSort(a, n);
    std::cout << "checking for sorted array: " << checkSorted(a, n) << std::endl;
    for (int i = 0; i < n; i++)
        std::cout << a[i] << std::endl;
    return 0;
}*/