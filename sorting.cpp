#include <iostream>

class comparable
{
public:
    virtual int compareTo(comparable& to) = 0;
};

class Integer : public comparable
{
    int i;

public:
    Integer(int k) : i{k} {}

    int getValue() { return i; }
    bool operator<(Integer &j) { return (i < j.getValue()); }
    bool operator>(Integer &j) { return (i > j.getValue()); }

    int compareTo(comparable& to)
    {
        if (*this < to)
            return -1;
        else if (*this > to)
            return 1;
        else
            return 0;
    }
};

bool less(comparable& a, comparable& b)
{
    return a.compareTo(b);
}

int main()
{
    auto i{Integer(5)};
    auto j{Integer{6}};

    std::cout << less(i, j) << std::endl;
}