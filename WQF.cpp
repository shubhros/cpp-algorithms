#include <iostream>
#include "WFQ.h"

int main(void)
{
    int n;
    int n1, n2;
    int already_connected = 0;
    int total_connections = 0;
    std::cin >> n;
    std::cout << "total nodes: " <<n << std::endl;
    WeightedQuickUnionQF uf(n);

    while (std::cin >> n1) {
        std::cin >> n2;
        total_connections++;
        if (!uf.is_connected(n1, n2))
            uf.connect(n1, n2);
        else {
            already_connected++;
            //std::cout << "already connected: " << n1 << " " << n2 << std::endl;
        }
    }

    //std::cout << "Already connected: " << already_connected << " (std::endl;
    printf("Already connected: %d (%d%%)\n", already_connected, (100 * already_connected)/total_connections);

    return 0;
}