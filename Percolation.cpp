#include <iostream>
#include "WFQ.h"
#include <cstdlib>
#include <ctime>
#include <vector>
#include <cmath>

class Percolation {
private:
    bool **grid;
    WeightedQuickUnionQF uf;
    int grid_size;
    int opened = 0;

    int grid_to_array(int row, int col)
    {
        return ((row - 1)*grid_size + col);
    }

public:
    Percolation(int n) : grid_size {n}, uf {WeightedQuickUnionQF(n*n + 2)}
    { 
        grid = new bool*[n];
        for (auto i = 0; i < n; i++) {
            grid[i] = new bool[n];
        }
        for (auto i = 0; i < n; i++) {
            for (auto j = 0; j < n; j++) {
                grid[i][j] = false; // initially all sites are closed
            }
        } 
        // connect the virtual sites
        for (auto i = 0; i < n; i++)
            uf.connect(0, i);
        for (auto i = n*n-n+1; i < n*n; i++)
            uf.connect(i, n*n+1);
    }
    ~Percolation()
    {
        for (auto i = 0; i < grid_size; i++) {
            delete[] grid[i];
        }
        delete[] grid;
    }
    bool is_open(int row, int col)
    {
        return grid[row - 1][col - 1];
    }
    void open(int row, int col)
    {
        if (is_open(row, col))
            return;
        // open the site
        grid[row - 1][col -1] = true;
        auto cell = grid_to_array(row, col);
        // connect the surrouding sites if they are open
        if (col != grid_size) {
			if (is_open(row, col + 1)) {
				uf.connect (cell, cell + 1);
			}
		}
		if (col != 1) {
			if (is_open(row, col -1)) {
				uf.connect (cell, cell - 1);
			}
		}
		if (row != grid_size) {
			if (is_open(row + 1, col)) {
				uf.connect (cell, cell + grid_size);
			}
		}
		if (row != 1) {
			if (is_open(row - 1, col)) {
				uf.connect (cell, cell - grid_size);
			}
		}
        opened++;
    }
    bool percolates()
    {
        return uf.is_connected(0, grid_size*grid_size + 1);
    }
    int number_of_opened_sites()
    {
        return opened;
    }
};

class PercolationStats {
private:
    std::vector<double> results;
    int trials;
    int n;
    int experiment(int gridSize)
    {
        Percolation p = Percolation(gridSize);
        srand((unsigned)time(0));
        while (!p.percolates()) {
            auto row = (rand() % gridSize) + 1;
            auto col = (rand() % gridSize) + 1;
            p.open(row, col);
        }
        return p.number_of_opened_sites();
    }
public:
    PercolationStats(int gridSize, int numTrials) 
        :n {gridSize}, trials{numTrials}
    {
        for (auto i = 0; i < numTrials; i++) {
            results.push_back(((double)experiment(n)) / (n * n));
        }
    }
    ~PercolationStats()
    {
    }
    double mean()
    {
        double sum = 0.0;
        for (auto v : results) {
            sum += v;
        }
        return (sum/trials);
    }

    double stddev()
    {
        auto m{mean()};
        double sum = 0.0;
        for (auto v : results) {
            sum += (v - m) * (v - m);
        }
        return (sqrt(sum/(trials - 1)));
    }

    double confidenceLo()
    {
        return (mean() - (1.96*stddev())/sqrt(trials));
    }

    double confidenceHi()
    {
        return (mean() + (1.96*stddev())/sqrt(trials));
    }
};

/*int main()
{
    int grid_size = 20;

    Percolation p = Percolation(grid_size);
    srand((unsigned)time(0));

    while(!p.percolates()) {
        auto row = (rand() % grid_size) + 1;
        auto col = (rand() % grid_size) + 1;
        p.open(row, col);
    }
    printf("Number of open sites: %d ratio: %.2f\n", p.number_of_opened_sites(),
           (1.0 * p.number_of_opened_sites()) / (grid_size * grid_size));
}*/

int main(int argc, char **argv)
{
    int n = atoi(argv[1]);
    int trials = atoi(argv[2]);

    PercolationStats s = PercolationStats(n, trials);
    printf("mean: %lf\n", s.mean());
    printf("stddev: %lf\n", s.stddev());
    printf("95%% confidence interval [%lf, %lf]\n", s.confidenceLo(), s.confidenceHi());
    return 0;
}